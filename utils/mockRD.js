const crypto = require("crypto");
const xmlParser = require("xml2json");
const config = require("../config.js");
const R = require("ramda");
const fs = require('fs');
const path = require('path');
const devicekey = fs.readFileSync(path.join(__dirname, '../keys') + "/devicePrivateKey.pem")
const pidEncryptionUtils = require("./pidEncryptionUtils");
require('colors');

const pidDataJson = {
    "PidData": {
        "Resp": {
            "errCode": "0",
            "errInfo": "",
            "fCount": "",
            "fType": "",
            "iCount": "",
            "iType": "",
            "pCount": "",
            "pType": "",
            "nmPoints": "",
            "qScore": ""
        },
        "DeviceInfo":config.deviceInfo
    }
};
const pidJson = {
    "Pid": {
        "ts": "",
        "ver": "",
        "wadh": "",
        "Demo": {},
        "Bios": {}
    }
};

const getType = pidOptionsJson => {
	return R.path(["Opts","fType"],pidOptionsJson)
		|| R.path(["Opts","iType"],pidOptionsJson)
		|| R.path(["Opts","pType"],pidOptionsJson)
		|| ""
}  

const getBios = (pidOptionsJson, ts) => {
  let bioData = R.path(["Bios", "Bio", "$t"], pidOptionsJson);
  if(!bioData) {
    if(typeof(R.path(["Bios"], pidOptionsJson))=="string")
      bioData = pidOptionsJson["Bios"];
    else
      bioData = "TestBioData";
  }
  let bh = new Buffer(crypto.createHash("sha256").update(bioData).digest("base64"),'base64');
  let sign = crypto.createSign('RSA-SHA256');
  let data = bh + ts + config.deviceInfo.dc
  let be = sign.update(data);
  let bs = sign.sign(devicekey, 'base64');
  let dih = (config.deviceInfo.dpId + config.deviceInfo.rdsId + 
      config.deviceInfo.rdsVer + config.deviceInfo.dc + config.deviceInfo.mi + "test");
  return {
    "dih":new Buffer(crypto.createHash("sha256").update(dih).digest("binary"), "ascii").toString("base64"),
		"Bio": {
			"bs": bs,
			"type": R.path(["Bios","Bio", "type"],pidOptionsJson) || "",
			"posh": R.path(["Bios","Bio", "posh"],pidOptionsJson) || "UNKNOWN",
			"$t": bioData
		}
	}
};       

const getPidXml = (pidOptionsXml,ts) => {
	let pidOptionsJson = JSON.parse(xmlParser.toJson(pidOptionsXml)).Pidson;
	let pid = pidData.Pid;
	pid.ts = ts;
	pid.wadh = R.path(["Opts","wadh"],pidOptionsJson) || "";
	pid.ver = R.path(["Opts","pidVer"],pidOptionsJson) || config.pidVersion;
	pid.Demo = pidOptionsJson.Demo;
	if(R.path(["Opts","otp"],pidOptionsJson) && R.path(["Opts","otp"],pidOptionsJson)!="") {
		pid.Pv = {
			otp:R.path(["Opts","otp"],pidOptionsJson)
		}
	}
	if(getType(pidOptionsJson)!="") {
		pid.Bios = getBios(pidOptionsJson, ts);
	}
  pidData = prunePidData(pidData)
  return xmlParser.toXml(pidData);
};

const prunePidData = (pidData)=>{
	var keys = Object.keys(pidData.Pid);
	for(var i in keys){
		if(Object.keys(pidData.Pid[keys[i]]).length === 0){
			delete pidData.Pid[keys[i]]
		}
	}
	return pidData;
}

const getPidDataXml = (pidOptionsXml) => {
	let pidJson = R.clone(pidDataJson);
	let pidData = pidJson.PidData;
	let ts = new Date().toISOString();
	ts = ts.substring(0,ts.length-5);
	let pidXml = getPidXml(pidOptionsXml,ts);
	let sessionKey = pidEncryptionUtils.getSessionKey();
	pidData.Skey = pidEncryptionUtils.getEncryptedSessionKey(sessionKey);
	pidData.Data = pidEncryptionUtils.getEncryptedPid(pidXml, ts, sessionKey);
	pidData.Hmac = pidEncryptionUtils.getHmacPid(pidXml, ts, sessionKey);
	return xmlParser.toXml(pidJson);
};

exports.capture = getPidDataXml;
exports.getPidDataXml = getPidDataXml;