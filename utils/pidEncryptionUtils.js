 const crypto = require("crypto");
const NodeRSA = require('node-rsa');
const fs = require("fs");
const signedXml = require('xml-crypto').SignedXml;
const FileKeyInfo = require('xml-crypto').FileKeyInfo;
const xmlBuilder = require("xmlbuilder");
const path = require("path");
const xmlParser = require("xml2json");
const R = require("ramda");
const signingKey = fs.readFileSync(path.join(__dirname, '../keys') + "/privateKey.pem");
const aesAlgorithm = "aes-256-gcm";
const aesKeySize = 32;
const notAfter = "20200916";
const publicKey = new NodeRSA(fs.readFileSync(path.join(__dirname, '../keys') + "/pubkey.pem").toString(),"pkcs8-public-pem",{
  encryptionScheme:"pkcs1"
});
const subjectName = "CN=Public AUA for Staging Services,  OU=Staging Services,O=Public AUA,L=Bangalore,ST=KA,C=IN";
const cert = fs.readFileSync(path.join(__dirname, '../keys') + "/cert").toString();

const getSessionKey = () => {
  return crypto.randomBytes(aesKeySize);
}

const getEncryptedSessionKey = (sessionKey) => {
  return {
    "ci": notAfter,
    "$t": publicKey.encrypt(sessionKey, 'base64')
  }
}

const getEncryptedPid = (pid, ts, sessionKey) => {
  ts = new Buffer(ts);
  let iv = ts.slice(ts.length - 12);
  let aad = ts.slice(ts.length - 16);
  let cipher = crypto.createCipheriv(aesAlgorithm, sessionKey, iv)
  cipher.setAAD(aad);
  let encrypted = cipher.update(pid, 'utf8', 'binary')
  encrypted += cipher.final('binary');
  var tag = cipher.getAuthTag();
  let encPid = Buffer.concat([new Buffer(encrypted,"ascii"), tag]);
  return { "type": "X", "$t":Buffer.concat([ts,encPid]).toString("base64") };
};

const getHmacPid = (pid, ts, sessionKey) => {
  ts = new Buffer(ts);
  let iv = ts.slice(ts.length - 12);
  let aad = ts.slice(ts.length - 16);
  let result = new Buffer(crypto.createHash("sha256").update(pid).digest("binary"), "ascii");
  let cipher = crypto.createCipheriv(aesAlgorithm, sessionKey, iv)
  cipher.setAAD(aad);
  let encrypted = cipher.update(result, 'utf8', 'binary')
  encrypted += cipher.final('binary');
  var tag = cipher.getAuthTag();
  let encPid = Buffer.concat([new Buffer(encrypted,"ascii"), tag]);
  return { "$t": encPid.toString("base64") };
};

exports.getEncryptedPid = getEncryptedPid;
exports.getHmacPid = getHmacPid;
exports.getEncryptedSessionKey = getEncryptedSessionKey;
exports.getSessionKey = getSessionKey;