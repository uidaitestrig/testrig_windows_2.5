const config = require("./config");
const ASALK = config.asaLicenseKey;
exports.AUTHURL = config.authHost+"/public/{0}/{1}/"+ASALK;
exports.FAILED = "FAILED";
exports.SUCCESS = "SUCCESS";
exports.RDHOST = config.rdHost;
exports.RDPORT = config.rdPort;
exports.RDVERB = config.rdVerb;
exports.DEVICEINFOVERB = config.deviceInfoVerb;
exports.CAPTUREVERB = config.captureVerb;

exports.reasons = {
	InvalidTType:"Invalid Test Type",
	EmptyPResponse:"Empty response from test executor",
	EmptyAssertResponse:"Empty response from test assert",
	EmptyRDParams:"Missing rd params in test config",
	EmptyRDInfo:"Empty response from rd service",
	GenericDeviceInfoFailed:"Error getting device info",
	MissingRDInfoPath:"Missing info path in RD Service",
	MissingRDCapturePath:"Missing capture path in RD Service",
	EmptyDeviceInfo:"Empty device info",
	GenericRDInfoFailed:"Error getting rd info",
	EmptyCaptureResult:"Empty capture result",
	EmptyCaptureResult:"Empty auth result",
	GenericCaptureFailed:"Generic capture result error",
	GenericTestFailure:"Error while executing test",
	ErrorCodeMismatch:"Error code mismatch"
}