const config = require("../config.js");

const authJson = {
  "Auth": {
    "xmlns": "http://www.uidai.gov.in/authentication/uid-auth-request/2.0",
    "ac":config.auaCode,
    "sa":config.subAuaCode,
    "ver":config.authVersion,
    "lk":config.licenseKey,
    "Meta": {
     // "udc": config.udc
    }
  }
}

const uses = {
   pi:"n",
   pa:"n",
   pfa:"n",
   bio:"n",
   bt:"n",
   pin:"n",
   otp:"n"
};

const deviceInfoAssertKeys = [{
  key:"dpId",
  defaultValue:".*"
},{
  key:"rdsId",
  defaultValue:".*"
},{
  key:"rdsVer",
  defaultValue:".*"
},{
  key:"dc",
  defaultValue:".*"
},{
  key:"mi",
  defaultValue:".*"
},{
  key:"mc",
  defaultValue:".*"
}]

exports.deviceInfoAssertKeys = deviceInfoAssertKeys; 
exports.authJson = authJson;
exports.uses = uses;