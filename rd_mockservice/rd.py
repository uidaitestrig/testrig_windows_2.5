from flask import Flask, make_response,request
from flask_cors import CORS, cross_origin

rdsXML = '<RDService status="READY" info="provider info for display purposes"><Interface id="CAPTURE" path="/capture" /><Interface id="DEVICEINFO" path="/info" /></RDService>'
infoXML = '<DeviceInfo dpId="TEST" rdsId="" rdsVer="" dc="" mi="" mc=""></DeviceInfo>';
captureXML = '<PidData><Resp errCode="0" errInfo="" fCount="" fType="" iCount="" iType="" pCount="" pType="" nmPoints="" qScore=""/><DeviceInfo /><Skey ci="">apqwipa</Skey><Hmac>pqikQoq</Hmac><Data type="X|P">poqqiw==</Data></PidData>';

app = Flask(__name__)
CORS(app,resources={r"*": {"methods": ["RDSERVICE","OPTIONS"]}})

@app.route('/', methods=['RDSERVICE'])
def rds():
    response = make_response(rdsXML)
    response.headers['Content-Type'] = 'text/xml; charset=utf-8'
    return response

@app.route('/info', methods=['DEVICEINFO'])
def info():
    response = make_response(infoXML)
    response.headers['Content-Type'] = 'text/xml; charset=utf-8'
    return response

@app.route('/capture', methods=['CAPTURE'])
def capture():
    print "DATA", request.data
    print "HEADERS", request.headers
    response = make_response(captureXML)
    response.headers['Content-Type'] = 'text/xml; charset=utf-8'
    return response

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=11110)