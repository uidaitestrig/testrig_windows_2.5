import React from 'react';
import { AppBar } from 'react-toolbox/lib/app_bar';
import {Tab, Tabs} from 'react-toolbox';
import Input from 'react-toolbox/lib/input';
import Card from 'react-toolbox/lib/card';
import Button from 'react-toolbox/lib/button';

import Adhoc from '../component/Adhoc';
import Batch from '../component/Batch'
import Rapid from '../component/Rapid'

import Styles from '../styles/appbar.css';
import CommonStyles from '../styles/common.css';

class App extends React.Component {
  componentWillMount() {
    this.state = {
      tabIndex: 0,

      setup: false,
      host: "127.0.0.1",

      licenseKey: "",
      asaLicenseKey: "",
      auaCode: "",
      authVersion: "",
      subAuaCode: "",
      authHost: "",
      udc: "",
      certPath: "",
      privateKeyPath: "",
      timeout: "",
      deviceId: "",
      deviceSerial: "",
      rdDetails: ""
    }
  }

  handleChange(key, value){
    let state = {};
    state[key] = value;
    this.setState(state);
  }

  view() {
    let authParams = {
      host: this.state.host,
      lk: this.state.licenseKey,
      ac: this.state.auaCode,
      sa: this.state.subAuaCode,
      ver: this.state.authVersion,
      Meta: {
        udc: this.state.udc,
      }
    };

    let options = {
      asalk: this.state.asaLicenseKey,
      authHost: this.state.authHost,
      certPath: this.state.certPath,
      privateKeyPath: this.state.privateKeyPath,
      timeout: this.state.timeout
    };

    let deviceDetails = {deviceId: this.state.deviceId, deviceSerial: this.state.deviceSerial, rdDetails: this.state.rdDetails};

    if(this.state.tabIndex == 0)
      return (<Adhoc options={options}  host={this.state.host} authParams={authParams}/>);
    else if(this.state.tabIndex == 1)
      return (<Batch  deviceDetails={deviceDetails} host={this.state.host}/>);
    else
      return (<Rapid deviceDetails={deviceDetails} options={options} host={this.state.host} authParams={authParams}/>);
  }

  showSetup() {
    let disabled = false;
    let keys = ["host"];
    for(let i=0; i<keys.length; i++){
      if(this.state[keys[i]] == "" || !this.state[keys[i]]) {
        disabled = true;
        break;
      }
    }

    return (
      <div style={{paddingLeft: "200px", paddingRight: "200px"}}>
        <Card style={{padding: "10px"}}>
          <span style={{fontSize: "20px", marginBottom: "10px"}}>
            Setup
          </span>
          <div style={{display: "flex"}}>
            <div className={CommonStyles.flex33}>
              <Input type='text' label={'RD Hostname'} name='host' value={this.state.host}
                onChange={this.handleChange.bind(this, 'host')} />
            </div>
            <div className={CommonStyles.flex33}>
              <Input type='text' label={'Auth Hostname'} name='authHost' value={this.state.authHost}
                onChange={this.handleChange.bind(this, 'authHost')} />
            </div>
            <div className={CommonStyles.flex33}>
              <Input type='number' label={'Timeout (ms)'} name='timeout' value={this.state.timeout}
                onChange={this.handleChange.bind(this, 'timeout')} />
            </div>
          </div>

          <div style={{display: "flex"}}>
            <div className={CommonStyles.flex50}>
              <Input type='text' label={'License Key'} name='licenseKey' value={this.state.licenseKey}
                onChange={this.handleChange.bind(this, 'licenseKey')} />
            </div>
            <div className={CommonStyles.flex50}>
              <Input type='text' label={'ASA License Key'} name='asaLicenseKey' value={this.state.asaLicenseKey}
                onChange={this.handleChange.bind(this, 'asaLicenseKey')} />
            </div>
          </div>

          <div style={{display: "flex"}}>
            <div className={CommonStyles.flex50}>
              <Input type='text' label={'Cert Path (absolute path)'} name='cert' value={this.state.certPath}
                onChange={this.handleChange.bind(this, 'certPath')} />
            </div>
            <div className={CommonStyles.flex50}>
              <Input type='text' label={'Private Key path (absolute path)'} name='privateKeyPath' value={this.state.privateKeyPath}
                onChange={this.handleChange.bind(this, 'privateKeyPath')} />
            </div>
          </div>

          <div style={{display: "flex"}}>
            <div className={CommonStyles.flex25}>
              <Input type='text' label={'auaCode'} name='auaCode' value={this.state.auaCode}
                onChange={this.handleChange.bind(this, 'auaCode')} />
            </div>
            <div className={CommonStyles.flex25}>
              <Input type='text' label={'subAuaCode'} name='subAuaCode' value={this.state.subAuaCode}
                onChange={this.handleChange.bind(this, 'subAuaCode')} />
            </div>
            <div className={CommonStyles.flex25}>
              <Input type='text' label={'authVersion'} name='authVersion' value={this.state.authVersion}
                onChange={this.handleChange.bind(this, 'authVersion')} />
            </div>
            <div className={CommonStyles.flex25}>
              <Input type='text' label={'UDC'} name='udc' value={this.state.udc}
                onChange={this.handleChange.bind(this, 'udc')} />
            </div>
          </div>
          <span style={{fontSize: "16px", marginBottom: "10px"}}>
            Device Details
          </span>
          <div style={{display: "flex"}}>
            <div className={CommonStyles.flex33}>

              <Input type='text' required={true} label={'Device id'} name='deviceId' value={this.state.deviceId}
                onChange={this.handleChange.bind(this, 'deviceId')} />
            </div>
            <div className={CommonStyles.flex33}>
              <Input type='text' required={true} label="Device Serial" name='deviceSerial' value={this.state.deviceSerial}
                onChange={this.handleChange.bind(this, 'deviceSerial')} />
            </div>
            <div className={CommonStyles.flex33}>
              <Input type='text' required={true} label="Rd Details eg: ver:1.0,os:windows" name='rdDetails' value={this.state.rdDetails}
                onChange={this.handleChange.bind(this, 'rdDetails')} />
            </div>
          </div>
          <div>
            <Button style={{float: "right"}} raised primary disabled={disabled} label="Done" onClick={()=>{this.setState({setup: true})}}/>
          </div>
        </Card>
      </div>
    );
  }

  render() {
    let view;
    if(!this.state.setup){
      view = this.showSetup();
    }else{
      view = this.view();
    }

    return (
      <div>
        <AppBar leftIcon="fingerprint" title="UIDAI Authentication Client" style={{height: "10px"}} flat>
          { this.state.setup &&
            <Tabs index={this.state.tabIndex} className={Styles.appbarTabs} onChange={(index)=>{this.setState({tabIndex: index})}} theme={Styles} fixed>
              <Tab label='Adhoc'/>
              <Tab label='Batch'/>
              <Tab label='Rapid'/>
            </Tabs>
          }
        </AppBar>
        <section style={{ padding: 20 }}>
          {view}
        </section>
      </div>
    );
  }
}

export default App;
