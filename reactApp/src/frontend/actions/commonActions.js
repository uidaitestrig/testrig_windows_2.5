const PORT_START = 11100;
const PORT_END = 11120;
const PROXY_HOST = "127.0.0.1:8091";
const RD_VERB = "RDSERVICE";
const RD_HOST = "127.0.0.1";

const Request = require('superagent');

function discoverRd(callback,rdHost) {
  for(let i=PORT_START; i<=PORT_END; i++) {
    let url =  "http://"+PROXY_HOST+"/rdInfo";
    let req =  Request.post(url);
    let data = { rdHost:rdHost || RD_HOST, rdVerb:RD_VERB, rdPort:i };
    req.port = i;
    req.send(data);
    req.end(getEndCB(i,rdHost || RD_HOST,callback));
  }
}

const getEndCB = (port,rdHost,callback) => {
  return  (err, result)=> {
    callback(err, result, port, rdHost);
  }
}

async function info(host, port, callback) {
  let url = "http://"+PROXY_HOST+"/tests";
  let req = Request.post(url);
  let data = {rdHost:host, rdVerb:RD_VERB, rdPort:port, name:"DEVICE_INFO", type:"DEVICE_INFO"};
  req.send(data);
  req.end((err,result)=>{
    callback(err, result);
  });
}

function capture(host, port, options, callback) {
  let url = "http://"+PROXY_HOST+"/tests";
  let req = Request.post(url);
  options.rdHost = host
  options.rdVerb = RD_VERB
  options.rdPort = port
  options.name = "RD_CAPTURE"
  options.type = "RD_CAPTURE";
  req.send(options);
  req.end((err,result)=> {
    callback(err, result);
  });
}

function authenticate(host, port, options, callback) {
  let url = "http://"+PROXY_HOST+"/tests";
  let req = Request.post(url);
  options.rdHost = host
  options.rdVerb = RD_VERB
  options.rdPort = port
  options.name = "AUTH"
  options.type = "AUTH";
  req.send(options);
  req.end((err,result)=> {
    callback(err, result);
  });
}

function auth(data) {
  let url = "http://"+PROXY_HOST+"/tests";
  let req = Request.post(url);
  return req.send(data);
}

export default {
  discoverRd,
  info,
  capture,
  authenticate,
  auth,
}