import React from 'react';

import Card from 'react-toolbox/lib/card';
import Input from 'react-toolbox/lib/input';
import Button from 'react-toolbox/lib/button';
import DatePicker from 'react-toolbox/lib/date_picker';
import Dropdown from 'react-toolbox/lib/dropdown';
import Dialog from 'react-toolbox/lib/dialog';
import JSONTree from 'react-json-tree'
import ProgressBar from 'react-toolbox/lib/progress_bar';
import { Snackbar } from 'react-toolbox';


import fileDownload from 'react-file-download';
import Actions from '../actions/commonActions';
import R from 'ramda';
import Types from '../utils/types';
import Utils from '../utils/helper';

import Styles from '../styles/common.css';

export default class PiView extends React.Component {

  defaultState() {
    let state = {
      aadhaarNumber: "",
      name: "",
      nameMatchValue: "",
      piMatchStrategy: "",
      localName: "",
      localMatchValue: "",
      gender: "",
      phone: "",
      age: "",
      DOB: null,
      DOBT: "",
      email: "",

      pin: "",
      channel: "",

      biometrics: "",
      pfaMatchStrategy: "",
      pfaAddress: "",
      pfaLocalAddress: "",
      pfaAddressMatchValue: "",
      pfaLocalAddressMatchValue: "",

      careOf: "",
      building: "",
      landmark: "",
      street: "",
      locality: "",
      poName: "",
      village: "",
      subdist: "",
      district: "",
      state: "",
      country: "",
      pincode: "",

      fCount: "",
      fType: "",
      iCount: "",
      iType: "",
      pCount: "",
      pType: "",
      format: "",
      pidVer: "",
      wadh: "",
      posh: "",
      otp: "",
      timeout:"",
      customOptions: "",
      output: [],
      assertStatus: "",
      assertCode: "",
    };
    return state;
  }

  componentWillMount() {
    this.state = this.defaultState();
    this.state["selectedRd"] = -1;
    this.state["rds"] = [];

    this.state["error"] = false;
    //Count of number of loading operations, 21 here as 21 RD ports have to be loaded
    this.state["loading"] = 0;

    this.state["message"] = "";

    this.state["snackbar"] = false;
    Actions.discoverRd(this.checkRd.bind(this),this.props.host);
  }

  resetResponse() {
    this.setState({output: [], assertStatus: ""});
  }

  reset() {
    this.setState(this.defaultState());
  }

  setLoading(value, message) {
    this.setState({loading: value, message: message});
  }

  showError(message){
    this.setState({error:true, message: message});
  }

  showSnackbar(message) {
    this.setState({snackbar:true, message: message});
  }

  hasKeys(data) {
    return Object.keys(data).length>0;
  }

  handleChange(key, value){
    let state = {};
    state[key] = value;
    this.setState(state);
  }

  export() {
    if(this.state.output != '')
      fileDownload(JSON.stringify(this.state.output), 'adhoc_test.json');
  }

   checkRd(err, data, port, host) {
    if(err)
      return;

    let obj = JSON.parse(data.text);

    if(obj.status=="FAILED")
      return;

    let arr = [...this.state.rds];
    let val = arr.length ;
    let selected = this.state.selectedRd;
    if(selected == -1) {
      selected = 0;
    }
    arr.push({value:val, data: {port: port, host:host}, label: "Port:"+port+" Status:" + obj.status});
    this.setState({rds: arr, selectedRd: selected});
  }

  captureDevice() {
    this.resetResponse();

    if(this.state.selectedRd == -1) {
      this.showError("No RD Device selected");
      return;
    }

    this.setLoading(1, "Capturing device, please wait");

    let pi = Utils.buildPi(this.state);
    let pa = Utils.buildPa(this.state);
    let pfa = Utils.buildPfa(this.state);
    let options = {
      "rdParams":{},
      "authParams": R.clone(this.props.authParams),
      "options": this.props.options
    };
    let rdParams = options.rdParams;
    rdParams.Demo = {};

    if(this.hasKeys(pi)) {
      rdParams.Demo.Pi = pi;
    }

    if(this.hasKeys(pa)) {
      rdParams.Demo.Pa = pa;
    }

    if(this.hasKeys(pfa)) {
      rdParams.Demo.Pfa = pfa;
    }

    rdParams.Opts = Utils.buildOpt(this.state);;
    let func = (err, data) => {
      this.setLoading(0, "");
      if(err) {
        this.showError(err.message);
        return;
      }
      let pdata = JSON.parse(data.text);
      this.setState({
          output: pdata,
          assertStatus: pdata.assertResponse.status,
          assertCode: pdata.assertResponse.reason
      });
    }
    let rd = this.state.rds[this.state.selectedRd].data;
    Actions.capture(rd.host, rd.port , options, func);
  }

  authenticate() {
    this.resetResponse();

    let opts = Utils.buildOpt(this.state);

    if(this.state.selectedRd == -1 && this.hasKeys(opts)) {
      this.showError("No RD Device selected when Rd parameters have been given");
      return;
    }

    this.setLoading(1, "Authenticating, please wait");

    let pi = Utils.buildPi(this.state);
    let pa = Utils.buildPa(this.state);
    let pfa = Utils.buildPfa(this.state);
    let options = {
      "rdParams":{},
      "authParams": R.clone(this.props.authParams),
      options: this.props.options
    };
    let rdParams = options.rdParams;
    let authParams = options.authParams;
    rdParams.Demo = {};

    if(this.hasKeys(pi)) {
      rdParams.Demo.Pi = pi
    }

    if(this.hasKeys(pa)) {
      rdParams.Demo.Pa = pi
    }

    if(this.hasKeys(pfa)) {
      rdParams.Demo.Pfa = pi
    }

    rdParams.Opts = opts;
    authParams.uid = this.state.aadhaarNumber;

    let port = 0;
    let host = 0;

    if(this.state.selectedRd != -1) {
      port = this.state.rds[this.state.selectedRd].data.port;
      host = this.state.rds[this.state.selectedRd].data.host;
    }

    let func = (err, data) => {
      this.setLoading(0, "");

      if(err){
        this.showError(err.message);
        return;
      }

      let pdata = JSON.parse(data.text);
      this.setState({
          output: pdata,
          assertStatus: pdata.assertResponse.status,
          assertCode: pdata.assertResponse.reason
      });
    }

    Actions.authenticate(host, port, options, func);
  }

  infoClick() {
    this.resetResponse();

    if(this.state.selectedRd == -1) {
      this.showError("No RD Device selected");
      return;
    }

    this.setLoading(1, "Checking Information, please wait");

    let func = (err, data) => {
      console.log("hererer");
      this.setLoading(0, "");

      if(err){
        this.showError(err.message);
        return;
      }

      let pdata = JSON.parse(data.text);
      this.setState({
          output: pdata,
          assertStatus: pdata.assertResponse.status,
          assertCode: pdata.assertResponse.reason
      });
    }

    let rd = this.state.rds[this.state.selectedRd].data;
    Actions.info(rd.host, rd.port, func);
  }

  rd() {
    return (
      <Card className={Styles.card}>
        <div className={Styles.cardTitle}>
          Pid Options
        </div>
        <div>
        <Dropdown
            auto
            label="Devices"
            onChange={this.handleChange.bind(this, 'selectedRd')}
            source={this.state.rds}
            value={this.state.selectedRd}
          />
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex33}>
            <Input type='text' label={'fCount'} name='fCount' value={this.state.fCount}
              onChange={this.handleChange.bind(this, 'fCount')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'iCount'} name='iCount' value={this.state.iCount}
              onChange={this.handleChange.bind(this, 'iCount')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'pCount'} name='pCount' value={this.state.pCount}
              onChange={this.handleChange.bind(this, 'pCount')} />
          </div>
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex33}>
            <Dropdown
              auto
              label="fType"
              onChange={this.handleChange.bind(this, 'fType')}
              source={Types.fType}
              value={this.state.fType}
            />
          </div>
          <div  className={Styles.flex33}>
            <Dropdown
              auto
              label="iType"
              onChange={this.handleChange.bind(this, 'iType')}
              source={Types.iType}
              value={this.state.iType}
            />
          </div>
          <div  className={Styles.flex33}>
            <Dropdown
              auto
              label="pType"
              onChange={this.handleChange.bind(this, 'pType')}
              source={[]}
              value={this.state.pType}
            />
          </div>
        </div>

        <div style={{display: "flex"}}>
          <div  className={Styles.flex33}>
            <Dropdown
              auto
              label="format"
              onChange={this.handleChange.bind(this, 'format')}
              source={Types.format}
              value={this.state.format}
            />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'pidVer'} name='pidVer'
              value={this.state.pidVer} onChange={this.handleChange.bind(this, 'pidVer')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='password' label={'OTP'} name='otp'
              value={this.state.otp} onChange={this.handleChange.bind(this, 'otp')} />
          </div>
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex33}>
            <Input type='text' label={'wadh'} name='wadh' value={this.state.wadh}
              onChange={this.handleChange.bind(this, 'wadh')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'posh'} name='posh'
              value={this.state.posh} onChange={this.handleChange.bind(this, 'posh')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'timeout'} name='timeout'
              value={this.state.timeout} onChange={this.handleChange.bind(this, 'timeout')} />
          </div>
        </div>
        <Input type='text' label={'Custom Options'} name='customOptions'
          value={this.state.customOptions} onChange={this.handleChange.bind(this, 'customOptions')} />
        <div style={{display: "flex"}}>
          <div className={Styles.flex50}>
             <Button label='Capture' onClick={this.captureDevice.bind(this)} raised primary style={{width: "100%"}}/>
          </div>
          <div className={Styles.flex50}>
             <Button label='Info' onClick={this.infoClick.bind(this)} raised primary style={{width: "100%"}}/>
          </div>
        </div>
      </Card>
    );
  }

  personal() {
    return (
      <Card className={Styles.card}>
        <div className={Styles.cardTitle}>
          Personal Information (Pi)
        </div>
        <div>
          <div style={{display: "flex"}}>
            <div  className={Styles.flex70}>
              <Input type='text' label={'Name'} name='name' value={this.state.name}
                onChange={this.handleChange.bind(this, 'name')} />
            </div>
            <div  className={Styles.flex30}>
              <Input type='number' label={'Match%'} name='nameMatchValue'
                value={this.state.nameMatchValue} onChange={this.handleChange.bind(this, 'nameMatchValue')} />
            </div>
          </div>

          <div style={{display: "flex"}}>
            <div className={Styles.flex70}>
              <Input  type='text' label={'localName'} name='localName' value={this.state.localName}
                onChange={this.handleChange.bind(this, 'localName')} />
            </div>
            <div className={Styles.flex30}>
              <Input type='number' label={'Match%'}
                name='localMatchValue' value={this.state.localMatchValue}
                onChange={this.handleChange.bind(this, 'localMatchValue')} />
            </div>
          </div>

          <div style={{display: "flex"}}>
            <div className={Styles.flex33}>
              <Dropdown
                auto
                label="Match Strategy"
                onChange={this.handleChange.bind(this, 'piMatchStrategy')}
                source={Types.matchStrategies}
                value={this.state.piMatchStrategy}
              />
            </div>
            <div className={Styles.flex33}>
              <Dropdown
                auto
                label="gender"
                onChange={this.handleChange.bind(this, 'gender')}
                source={Types.gender}
                value={this.state.gender}
              />
            </div>
            <div className={Styles.flex33}>
              <Dropdown
                auto
                label="DOBT"
                onChange={this.handleChange.bind(this, 'DOBT')}
                source={Types.dobt}
                value={this.state.DOBT}
              />
            </div>
          </div>
          <div style={{display: "flex"}}>
            <div className={Styles.flex50}>
              <Input type='email' label='Email address' icon='email' value={this.state.email} onChange={this.handleChange.bind(this, 'email')} />
            </div>
            <div className={Styles.flex50}>
              <Input type='tel' label='Phone' name='phone' icon='phone' value={this.state.phone} onChange={this.handleChange.bind(this, 'phone')} />
            </div>
          </div>
          <div style={{display: "flex"}}>
            <div  className={Styles.flex70}>
              <DatePicker label='Date of Birth' sundayFirstDayOfWeek maxDate={new Date()}
                onChange={this.handleChange.bind(this, 'DOB')} value={this.state.DOB} />
            </div>
            <div  className={Styles.flex30}>
              <Input type='text' label='age'
                name='Age' value={this.state.age}
                onChange={this.handleChange.bind(this, 'age')} />
            </div>
          </div>
        </div>
      </Card>
    );
  }

  pin(){
    return (
      <Card className={Styles.card}>
        <div className={Styles.cardTitle}>
          OTP
        </div>
        <div style={{display: "flex"}}>
          <div className={Styles.flex70}>
            <Dropdown
              auto
              label="channel"
              onChange={this.handleChange.bind(this, 'channel')}
              source={Types.channel}
              value={this.state.channel}
            />
          </div>
          <div className={Styles.flex30} style={{marginTop: "15px"}}>
            <Button style={{width: "100%"}} label='Generate' icon='textsms' raised primary />
          </div>
        </div>
      </Card>
    );
  }

  aadhaar() {
    return (
      <Card className={Styles.card}>
        <div style={{display: "flex"}}>
          <div className={Styles.flex60}>
            <Input type='text' label='Aadhaar Number' name='aadhaarNumber'
               value={this.state.aadhaarNumber} onChange={this.handleChange.bind(this, 'aadhaarNumber')} />
          </div>
          <div className={Styles.flex40} style={{marginTop: "20px"}}>
            <Button style={{width: "100%"}} icon='sync' onClick={this.authenticate.bind(this)} label='Authenticate' primary raised/>
          </div>
        </div>
      </Card>
    );
  }

  response() {
    let status;
    let resp = this.state.assertStatus;
    if(resp == '') {
      status = null;
    } else if(resp == 'SUCCESS') {
      status = (<Button label='SUCCESS' icon="check" style={{backgroundColor: "green"}} primary raised/>);
    } else{
      status = (<Button label={this.state.assertCode} icon="cancel" style={{backgroundColor: "red"}} primary raised/>);
    }

    let theme = {
      scheme: "simple",
      base00: '#ff',
    };

    let tree =  (
      <JSONTree theme={theme}
      shouldExpandNode={()=>{return true;}}
      data={this.state.output}
      invertTheme={true}/>
    );

    if(this.state.loading > 0) {
      return (
        <Card className={Styles.card}>
          <ProgressBar type="circular" mode="indeterminate" />
          {this.state.message}
        </Card>
      );
    }

    let disabled = (this.state.output instanceof Array)? true: false;

    return (
      <Card className={Styles.card}>
        <div style={{display: "inline-block"}}>
          <div className={Styles.cardTitle}  style={{float: "left", width: "auto"}}>
            {status}
          </div>
          <div style={{float: "right", width: "auto"}}>
            <Button icon='archive' disabled={disabled} label='Export JSON' onClick={this.export.bind(this)} raised primary />
          </div>
        </div>
        <div className={Styles.cardTitle} style={{marginRight:'30px', maxHeight: "400px", overflow: "auto"}}>
          {tree}
        </div>
      </Card>
    );
  }

  personalFullAddress() {
    return (
      <Card className={Styles.card}>
        <div className={Styles.cardTitle}>
          Personal FullAddress(Pfa)
        </div>
        <Dropdown
          auto
          label="Match Strategy"
          onChange={this.handleChange.bind(this, 'pfaMatchStrategy')}
          source={Types.matchStrategies}
          value={this.state.pfaMatchStrategy}
        />
        <div style={{display: "flex"}}>
          <div  className={Styles.flex70}>
            <Input rows={1} type='text' multiline={true} label={'Address'} name='pfaAddress' value={this.state.pfaAddress}
              onChange={this.handleChange.bind(this, 'pfaAddress')} />
          </div>
          <div  className={Styles.flex30}>
            <Input type='number' label={'Match%'} name='pfaAddressMatchValue'
              value={this.state.pfaAddressMatchValue} onChange={this.handleChange.bind(this, 'pfaAddressMatchValue')} />
          </div>
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex70}>
            <Input style={{width: "100%"}} rows={1} type='text' multiline={true} label={'Local Address'} name='pfaLocalAddress' value={this.state.pfaLocalAddress}
              onChange={this.handleChange.bind(this, 'pfaLocalAddress')} />
          </div>
          <div className={Styles.flex30}>
            <Input type='number' label={'Match%'} name='pfaLocalAddressMatchValue'
              value={this.state.pfaLocalAddressMatchValue} onChange={this.handleChange.bind(this, 'pfaLocalAddressMatchValue')} />
          </div>
        </div>
      </Card>
    );
  }

  address(){
    return (
      <Card className={Styles.card}>
        <div className={Styles.cardTitle}>
            Personal Address(Pa)
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex25}>
            <Input rows={1} type='text' label={'Care Of'} name='careOf' value={this.state.careOf}
              onChange={this.handleChange.bind(this, 'careOf')} />
          </div>
          <div  className={Styles.flex25}>
            <Input type='text' label={'Building'} name='building'
              value={this.state.building} onChange={this.handleChange.bind(this, 'building')} />
          </div>
          <div  className={Styles.flex25}>
            <Input type='text' label={'Landmark'} name='landmark'
              value={this.state.landmark} onChange={this.handleChange.bind(this, 'landmark')} />
          </div>
          <div  className={Styles.flex25}>
            <Input rows={1} type='text' label={'Street'} name='street' value={this.state.street}
              onChange={this.handleChange.bind(this, 'street')} />
          </div>
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex33}>
            <Input rows={1} type='text' label={'Village/Town/City'} name='village' value={this.state.village}
              onChange={this.handleChange.bind(this, 'village')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'Locality'} name='locality'
              value={this.state.locality} onChange={this.handleChange.bind(this, 'locality')} />
          </div>
          <div  className={Styles.flex33}>
            <Input type='text' label={'PO Name'} name='poName'
              value={this.state.poName} onChange={this.handleChange.bind(this, 'poName')} />
          </div>
        </div>
        <div style={{display: "flex"}}>
          <div  className={Styles.flex30}>
            <Input type='text' label={'Sub district'} name='subdist'
              value={this.state.subdist} onChange={this.handleChange.bind(this, 'subdist')} />
          </div>
          <div  className={Styles.flex30}>
            <Input type='text' label={'District'} name='district'
              value={this.state.district} onChange={this.handleChange.bind(this, 'district')} />
          </div>
          <div  className={Styles.flex20}>
            <Input type='text' label={'State'} name='state'
              value={this.state.state} onChange={this.handleChange.bind(this, 'state')} />
          </div>
          <div  className={Styles.flex20}>
            <Input type='number' label={'Pincode'} name='pincode'
              value={this.state.pincode} onChange={this.handleChange.bind(this, 'pincode')} />
          </div>
        </div>
      </Card>
    );
  }

  dialog() {
    let show = this.state.error;
    let func = ()=> {this.setState({error: false})}
    func = func.bind(this);
    let actions = [{ label: "Ok", onClick: func}];
    let title = "Error";

    return (
      <Dialog
        actions={actions}
        active={show}
        onEscKeyDown={func}
        onOverlayClick={func}
        title={title}>
        <div>
          {this.state.message}
        </div>
      </Dialog>
    );
  }

  render(){
    let rd = this.rd();
    let personal = this.personal();
    let pin = this.pin();
    let personalFullAddress = this.personalFullAddress();
    let address = this.address();
    let aadhaar = this.aadhaar();
    let response = this.response();
    let dialog = this.dialog();

    return (
      <div>
        <div style={{display: "flex"}}>
          <div style={{flexBasis: "33%", marginRight: "10px"}}>
            {rd}
          </div>
          <div style={{flexBasis: "33%", display: "flex", flexDirection: "column", marginRight: "10px"}}>
            <div style={{flexBasis: "78%", marginBottom: "10px"}}>
              {personal}
            </div>
            <div style={{flexBasis: "20%"}}>
              {pin}
            </div>
          </div>
          <div style={{flexBasis:"33%", display: "flex", flexDirection: "column"}}>
            <div style={{flexBasis: "18%", marginBottom: "10px"}}>
              {aadhaar}
            </div>
            <div style={{flexBasis: "82%"}}>
              {response}
            </div>
          </div>
        </div>
        <div style={{display: "flex", marginTop: "10px"}}>
          <div style={{flexBasis: "35%", marginRight: "10px"}}>
            {personalFullAddress}
          </div>
          <div style={{flexBasis:"65%"}}>
            {address}
          </div>
        </div>
        <div title="options" style={{position: "fixed", right: "20px", bottom: "20px"}}>
          <div>
            <Button icon='delete' onClick={this.reset.bind(this)} floating primary/>
          </div>
        </div>
        <Snackbar
          action='Hide'
          label={this.state.message}
          onClick={()=>{this.setState({snackbar: false});}}
          type='warning'
          active={this.state.snackbar}
          onTimeout={()=>{this.setState({snackbar: false});}}
          timeout={2000}
        />
        {dialog}
      </div>
    );
  }
}

