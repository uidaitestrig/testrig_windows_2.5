
export default {
  matchStrategies: [
    {value: "E", label: "Exact"},
    {value: "P", label: "Partial"}
  ],

  gender: [
    {value: "M", label: "Male"},
    {value: "F", label: "Female"},
    {value: "T", label: "Others"}
  ],

  channel: [
    {value: "Mobile", label: "Mobile"}, 
    {value: "Email", label: "Email"}
  ],

  dobt: [
    {value: "V", label: "Verified"}, 
    {value: "D", label: "Declared"},
    {value: "A", label: "Approx"}
  ],

  fType: [
    {value: 0, label: "FMR"},
    {value: 1, label: "FIR"}
  ],

  iType: [{value: 0, label: "IIR"}],

  format: [
    {value: 0, label: "XML"},
    {value: 1, label: "Protobuf"}
  ],
}