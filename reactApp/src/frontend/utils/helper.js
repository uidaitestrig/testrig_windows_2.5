function buildPi(state){
  let keys = [{name: "name"} , {name: "nameMatchValue", label: "mv"},
    {name: "localName", label: "lname"}, {name: "localMatchValue", label: "lmv"},
    {name: "phone"}, {name: "age"}, {name: "email"}, {name: "DOBT", label: "dobt"},
    {name: "piMatchStrategy", label: "ms"}, {name: "gender"}];
  let pi = {};
  for(let i in keys) { 
    let key = keys[i];
    if(state[key.name] != ""){
      let label = (key.label != undefined)?key.label : key.name;
      pi[label] = state[key.name];
    }
  }
  if(state.DOB != null) {
    pi["dob"] = this.state.DOB;
  }
  return pi;
}

function buildOpt(state) {
  let keys = [{name: "fCount"}, {name: "fType"}, {name: "iCount"}, 
    {name: "iType"}, {name: "pCount"}, {name: "pType"}, {name: "format"},
    {name: "pidVer"}, {name: "otp"}, {name: "wadh"}, {name: "posh"},{name:"timeout"}];
  let opts = {};
  for(let i in keys) { 
    let key = keys[i];
    if(state[key.name] != "" || (typeof(state[key.name])=="number" && state[key.name]==0)){
      let label = (key.label != undefined)?key.label : key.name;
      opts[label] = state[key.name];
    }
  }
  return opts;
}

function buildPa(state) {
  let keys = [{name:"careOf", label: "co"},{name:"building", label: "house"},
    {name:"street"}, {name:"locality", label: "loc"},
    {name:"village", label: "vtc"}, {name:"subdist", label: "subdist"},
    {name:"state"},{name:"country"},{name:"poName", label: "po"},
    {name:"pincode", label: "po"}];
  let pa = {};
  for(let i in keys) { 
    let key = keys[i];
    if(state[key.name] != ""){
      let label = (key.label != undefined)?key.label : key.name;
      pa[label] = state[key.name];
    }
  }
  return pa;
}

function buildPfa(state) {
  let keys = [{name: "pfaMatchStrategy",label: "ms"},
    {name: "pfaAddressMatchValue", label: "mv"},
    {name: "pfaAddress", label: "av"}, {name: "pfaLocalAddress", label: "lav"},
    {name: "pfaLocalAddressMatchValue", label: "lmv"}];
  let pfa = {};
  for(let i in keys) { 
    let key = keys[i];
    if(state[key.name] != ""){
      let label = (key.label != undefined)?key.label : key.name;
      pfa[label] = state[key.name];
    }
  }
  return pfa;
}

export default {
  buildPa,
  buildPfa,
  buildPi,
  buildOpt
};