let nameRegex = new RegExp(/^[a-z ,.'-]+$/i);
let matchValueRegex = new RegExp(/^(100|[0-9]{1,2})$/i)
let ageRegex = new RegExp(/^[0-9]{1,3}$/);
let fCountRegex = new RegExp(/^(10|[0-9])$/);
let iCountRegex = new RegExp(/^[0-2]$/);
let pCountRegex = new RegExp(/^[0-1]$/);

//let matchSingleWhiteSpaceCharacterRegex = new RegExp(/^[\S\s]*?$/);

function name(value) {
  if(!nameRegex.test(value))
    return "Invalid Name: " +value;
  return null;
}

function localName(value) {
  if(!nameRegex.test(value))
    return "Invalid LocalName: "+value;
  return null;
}

function age(value){
  if(!ageRegex.test(value)){
    return "Invalid age: "+value;
  }
  return null;
}

function matchValue(value) {
  if(!matchValueRegex.test(value)){
    return "MatchValue can have values between 0-100";
  }
  return null;
}

function fCount(value) {
  if(!fCountRegex.test(value)){
    return "Invalid fCount value: " + value;
  }
  return null;
}

function iCount(value) {
  if(!iCountRegex.test(value)){
    return "Invalid iCount value: " + value;
  }
  return null;
}

function pCount(value) {
  if(!pCountRegex.test(value)){
    return "Invalid pCount value: " + value;
  }
}

//Added function removeTag to use <matchSingleWhiteSpaceCharacterRegex> regex variable defined top of file to remove additional_info tag

/*
function removeTag(value) {
  if(!matchSingleWhiteSpaceCharacterRegex.test(value)){
    return "Invalid matchSingleWhiteSpaceCharacterRegex value: " + value;
  }
}
*/

export default {
  name,
  localName,
  age,
  fCount,
  iCount,
  pCount,
  pfaAddressMatchValue: matchValue,
  pfaLocalAddressMatchValue: matchValue,
  localMatchValue: matchValue,
  nameMatchValue: matchValue,
  //removeTag --uncomment to use this regex in other classes
};