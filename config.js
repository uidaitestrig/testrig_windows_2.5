module.exports = {
  "pidOptionsVersion": "1.0",
  "deviceInfo": {
    "dpId": "SECUGEN.SGI",
    "rdsId": "SGI.WIN.001",
    "rdsVer": "1.0.0",
    "dc": "9b795332-af9b-353c-a8bc-2eb22dd46eaf",
    "mi": "HU20",
    "mc": "MIIDojCCAoqgAwIBAgIGAVsjIgPZMA0GCSqGSIb3DQEBBQUAMHwxGDAWBgNVBAMMD3FhX21hbnVmYWN0dXJlcjEYMBYGA1UECwwPcWFfbWFudWZhY3R1cmVyMRgwFgYDVQQKDA9xYV9tYW51ZmFjdHVyZXIxEjAQBgNVBAcTCUJhbmdhbG9yZTELMAkGA1UECBMCS0ExCzAJBgNVBAYTAklOMB4XDTE3MDMzMTA2NTEyM1oXDTE4MDMzMTA2NTEyM1owajESMBAGA1UEAwwJcWFfZGV2aWNlMRIwEAYDVQQLDAlxYV9kZXZpY2UxEjAQBgNVBAoMCXFhX2RldmljZTESMBAGA1UEBxMJQmFuZ2Fsb3JlMQswCQYDVQQIEwJLQTELMAkGA1UEBhMCSU4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDho3HKvn1yUj9hIEXtkkUeomaFgs4qbyuXpiR5gGnZ4O8JAi5fu3vi6+I8LPb4pFNnPvfdcQ1bTALJV0Rm2BjE4vDQ/d9nic8Q+ADwrDR/CKyGQuaXg5i9s/+pB9o3IF6CF1f6ttnR0U1TnAOxf6vw1igW1ISA+npEX7wukKM12HBHPvFKTU1AeRofJVzyuFq0dmhBLa/Xf6TW5ura+YZXsO4FtgRAH8Ct+imUxLmiiGyyhj3F1OJrTuWJ2M7HWWSntFu/ZCGm5Bw6Fp60hKZfEe2rMnevr2QwDFatu8RxjcMsvQL936oaUVlERRVwfn8hMVb8gIkxwrzR5xbrMKTNAgMBAAGjPDA6MAwGA1UdEwQFMAMBAf8wCwYDVR0PBAQDAgGGMB0GA1UdDgQWBBST5/B8saFsm91i+0bE/a6VIUkKeDANBgkqhkiG9w0BAQUFAAOCAQEAblitlME4CD1mbGtdD7AjyXRBFdPxUPvjmWnJzYXbg3XP8jmWQGvejCHa2lICi0ubHMkLu6Mghd8jx81B4uQNS7VsKH5yRIZadLmB5lShtoH3aaChYat7P/n7oShKBQ7yRiBro+4SWw2o3XThGcnhKL4zZhb/ot+5KwyL2AiGSwz696g/NA0l7kJzZvKDdC12ufrgIKf6Z9SUmKaltQDauaRQ4uxZU8heIa9RI3SZsv0TCEfzHns/M+P3sL690vJbLWx+uI7p6BdTKZh0QdM6J1ZF5UmKObUrZaANNsbXcwkEldjBlglzYr+qQldusanfaMLCRFBrpcDU5xRfL9XMnA=="
  },
  "udc": "200",
  "licenseKey": "MBBmxXVg94w5kyUu-eiounIfzTdltSoGyIgm1e3DWiNXdm0sjeAImCw",
  "asaLicenseKey": "MFRxUpvhaDG0YCzDaPzAgUgzgp008xkY4xFijLvkGzV54Cd2ltgBDs8",
  "authVersion": "2.5",
  "subAuaCode": "STGTCSUBA2",
  "auaCode": "STGUIDAITC",
  
  
  
 // "licenseKey": "MC_YBecppdVNwG-o_Hgf0kV68gUVzydkoRcWRy_SgkleUYer15oWr7Q",
 // "asaLicenseKey": "MNyYXdDmjmTQWmDMesWr8IyglEhwc2tTSrHClIaz45-4mbny6zQOpVI",
 // "authVersion": "2.5",
 // "subAuaCode": "STGTCSUBA2",
  //"auaCode": "STGTCUIDAI",
  "authHost": "http://10.5.72.53:9000/uidauthserver",
  "dbHost": "http://developer.uidai.gov.in/amcs/objects/",
  "ret":"y",
  "captureRespErrorCode":"0",
  "rc":"Y",
  "port":"8091",
  "rdHost":"10.8.2.10",
  "rdPort":"11100",
  "rdVerb":"RDSERVICE",
  "deviceInfoVerb":"DEVICEINFO",
  "captureVerb":"CAPTURE",
  "pidVersion":"2.0"
}